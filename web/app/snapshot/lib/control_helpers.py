# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import urllib
from flask import request


def rfc3339_timestamp(timestamp):
    return timestamp.strftime('%Y%m%dT%H%M%SZ')


def build_url_archive(archive=None, timestamp=None, path=None, isadir=True):
    url = request.environ.get('SCRIPT_NAME') + "/"

    if not archive:
        return url

    url += 'archive/' + archive + "/"
    url = urllib.parse.quote(url)

    if not timestamp:
        return url

    url += rfc3339_timestamp(timestamp) + '/'

    if not path:
        return url

    # This condition is always true, parent methods make sure not to call
    # build_url_archive on '/'
    if path != '/':  # pragma: no branch
        for path_element in path.strip('/').split('/'):
            url += urllib.parse.quote(path_element) + '/'

    if not isadir:
        url = url.rstrip('/')

    return url


def link_quote_array(array):
    return map(lambda x: {'raw': x,
                          'quoted': urllib.parse.quote(x)}, array)


def get_domain():
    return request.host.rsplit(':', 1)[0]

# def modified_since(last_mod):
#     if last_mod is None:
#         return
#
#     if last_mod.tzinfo is None:
#         raise "*sigh* - how do I set tzinfo to UTC on a naive "
#               "datetime object?"
#         # last_mod = last_mod.replace(tzinfo=0)
#
#     if_since = None
#     if request.if_modified_since:
#         print "if mod since:",  request.if_modified_since
#         if_since = request.if_modified_since
#     elif request.if_unmodified_since:
#         print "if unmod since:",  request.if_unmodified_since
#         if_since = request.if_unmodified_since
#
#     if if_since and (last_mod <= if_since):
#         print "need not send"
#         raise HTTPNotModified()
#
#     response.last_modified = last_mod
