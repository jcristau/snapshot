# Deploy snapshot frontend

This document describe two different method of deployment, either using a
virtual environment (recommended for development and testing) or using Apache
webserver and system package (recommanded for production).

Otherwise specified, all instructions assumed you are located in snapshot
`web/app` directory.

## Requirements

In order to use snapshot frontend, you must already have a snapshot farm and
postgres database. You may setup on by following snapshot's installation
instruction in the README, at the root of the project.

## Virtual environment

### Setup

Install the following dependencies:

```bash
sudo apt install python3-venv libpq-dev build-essential python3-dev
```

Create a new virtual environment:

```bash
python3 -m venv venv
```

Each time you start a new shell and work on snapshot, activate the virtual
environment:

```bash
. ./venv/bin/activate
```

Install the project and its dependencies:

```bash
python setup.py develop
```

Finally, choose your settings and create the snapshot.py configuration file:

```bash
ln -sf develop.py snapshot/settings/snapshot.py
```

### Use

To run an snapshot frontend instance locally (assuming your venv is activated),
run:

```
FLASK_ENV=development flask run
```

The interface is available at http://localhost:5000/

### Test

Install testing dependencies:

```bash
sudo apt install tox aptly devscripts postgresql-13-debversion \
    postgresql-plperl ruby ruby-pg uuid-runtime
```

To run snapshot test suite, execute:

```bash
tox
```

You can find html coverage report under `htmlcov/index.html`

## Webserveur and system packages

### Setup

Install required dependencies:

```bash
sudo apt install apache2 libapache2-mod-wsgi-py3 python3-flask \
    python3-flask-mail python3-psycopg2
```

Setup snapshot configuration:

```bash
# Edit snapshot/settings/prod.py
ln -sf prod.py snapshot/settings/snapshot.py
```

Add apache configuration:

```bash
cat << EOF | sudo tee /etc/apache2/sites-available/snapshot.conf
<VirtualHost *:80>
    ServerName snapshot.debian.org

    DocumentRoot /srv/snapshot.debian.org/code/web/app/public
    Alias /robots.txt /srv/snapshot.debian.org/code/web/app/public/robots.txt
    Alias /static /srv/snapshot.debian.org/code/web/app/snapshot/static

    WSGIDaemonProcess snapshot user=snapshot group=snapshot threads=10
    WSGIProcessGroup snapshot
    WSGIScriptAlias / /srv/snapshot.debian.org/code/web/app/wsgi.py

    <Directory /srv/snapshot.debian.org/code/web/app/>
        Require all granted
    </Directory>

    ErrorLog \${APACHE_LOG_DIR}/snapshot-error.log
    CustomLog \${APACHE_LOG_DIR}/snapshot-access.log combined
</VirtualHost>
EOF
```

Make sure to adapt path and user/group to your setup.

Enable the site and reload apache:

```bash
a2ensite snapshot
apachectl configtest && systemctl reload apache2
```

Snapshot should now be up and running.
