PROJECT = 'snapshot'
VERSION = '1.0'
AUTHOR = 'Snapshot maintainers (see AUTHORS)'
COPYRIGHT = '2009-2020, {}'.format(AUTHOR)
