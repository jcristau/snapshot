# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2021 Baptiste Beauplat <lyknode@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from psycopg2.pool import PoolError
from pytest import raises

from snapshot.lib.dbinstance import DBInstance


def test_dbinstance_init_failed(app):
    app.pool.closeall()
    with raises(PoolError):
        DBInstance(app.pool)


def test_dbinstance_query_failed(app):
    with DBInstance(app.pool) as db:
        db.MAX_ERR = 1  # Reduce retries to speedup test
        app.pool.closeall()
        with raises(PoolError):
            db.query('SELECT 1')


def test_dbinstance_query_one_multiple_results(app):
    with DBInstance(app.pool) as db:
        with raises(Exception) as e:
            db.query_one('SELECT * FROM node')

        assert str(e) == 'Got more than one return in query_one'
