# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from os.path import realpath, abspath, join
from sys import path
from logging import getLogger

from testing.postgresql import PostgresqlFactory
from pytest import fixture
from psycopg2 import connect
from flask import Response

from tests.controllers.snapshot import SnapshotController, SnapshotDBController
from tests.controllers.archive import ArchiveController
from tests.controllers.archive_view import ArchiveViewController
from tests.controllers.package_view import PackageViewController
from tests.controllers.package import PackageController
from tests.controllers.dataset import DatasetController
from tests.controllers.breadcrumbs import BreadcrumbsController
from tests.controllers.gpg import GPGController
from tests.controllers.removal import RemovalController
from tests.controllers.file import FileController
from tests.data.dataset import SNAPSHOT_DATASET
from snapshot import create_app

path.append(realpath(join(abspath(__file__), '..', '..', '..', '..')))
from lib.dbhelper import DBHelper  # noqa: E402
from db.upgrade import upgrade  # noqa: E402

log = getLogger(__name__)
Response.autocorrect_location_header = True


def load_snapshot_db(postgresql):
    Snapshot.db = postgresql.url()
    db = DBHelper(postgresql.url())
    db.execute('ROLLBACK')

    # Extracted from db-init.sql
    db.execute('CREATE EXTENSION debversion;')
    db.execute('CREATE LANGUAGE plperl;')

    # Load the initial schema
    with open(realpath(join(
            abspath(__file__), '..', '..', '..', '..', 'db', 'db-create.sql'
            ))) as data:
        db.execute(data.read())

    # And upgrade until last version
    upgrade(db)
    db.close()

    # Initialize snapshot database and farm
    log.debug('Seeding snapshot')
    with PackageController(SNAPSHOT_DATASET) as packages:
        for timestamp, archives in SNAPSHOT_DATASET.items():
            for name, config in archives.items():
                with ArchiveController(name, config, packages) as archive:
                    Snapshot.snapshot(timestamp, archive)


@fixture(scope="session", autouse=True)
def cleanup(request):
    def cleanup():
        # clear cached database at end of tests
        Postgresql.clear_cache()
        Snapshot.cleanup()
    request.addfinalizer(cleanup)


@fixture
def postgresql():
    with Postgresql() as db:
        yield db


@fixture
def db(postgresql):
    with connect(**postgresql.dsn()) as db:
        with db.cursor() as cursor:
            yield cursor


@fixture
def app(postgresql):
    app = create_app({
        'DATABASE': {**postgresql.dsn()},
        'FARM_PATH': Snapshot.farm,
    })

    yield app


@fixture
def bad_app(app):
    app.pool = None
    app.testing = False

    return app


@fixture
def mailbox(app):
    with app.mail.record_messages() as mailbox:
        yield mailbox


@fixture
def client(app):
    return app.test_client()


@fixture
def snapshot():
    return Snapshot


@fixture
def removal(db):
    return RemovalController(db)


@fixture
def breadcrumbs():
    return BreadcrumbsController()


@fixture
def dataset():
    return DatasetController(SNAPSHOT_DATASET)


@fixture
def snapshot_db(db):
    return SnapshotDBController(db)


@fixture
def package_view(snapshot_db):
    return PackageViewController(snapshot_db)


@fixture
def archive_view(snapshot_db):
    return ArchiveViewController(snapshot_db)


@fixture
def file_ctrl():
    return FileController(Snapshot.farm)


GPG = GPGController()
Snapshot = SnapshotController()
Postgresql = PostgresqlFactory(cache_initialized_db=True,
                               on_initialized=load_snapshot_db,
                               initdb_args='-U postgres -A trust -E SQL_ASCII')
