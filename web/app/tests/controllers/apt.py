# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from glob import glob
from re import match
from os import environ
from os.path import basename
from subprocess import check_output
from shutil import rmtree


class AptController():
    def __init__(self, url, workdir):
        self.workdir = workdir
        self._setup_apt(url)

    def _setup_apt(self, url):
        self._run(['create', 'unstable',
                   f'[trusted=yes] {url}/archive/debian/now/',
                   'unstable', 'main'])
        self._run(['apt', 'unstable', 'update'])

    def _run(self, args, cwd=None):
        if not cwd:
            cwd = self.workdir

        env = environ.copy()
        env['HOME'] = self.workdir

        return check_output([
                                'chdist',
                            ] + args,
                            cwd=cwd,
                            text=True,
                            timeout=5,
                            env=env)

    def _lookup_files(self, workdir, extension):
        version = None

        for path in glob(str(self.workdir.join('dest', f'*.{extension}'))):
            print(f'{path}')
            version = match(fr'[^_]*_([^_]*).*\.{extension}',
                            basename(path)).group(1)

        return version

    def fetch(self, package_type, package):
        workdir = self.workdir.join('dest')

        workdir.mkdir()
        self._run(['apt', 'unstable', package_type, package], workdir)

        if package_type == 'source':
            version = (self._lookup_files(workdir, 'dsc'))
        else:
            version = (self._lookup_files(workdir, 'deb'))

        rmtree(workdir)

        return version
