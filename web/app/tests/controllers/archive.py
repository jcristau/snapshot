# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from os.path import isdir, join
from os import symlink
from shutil import rmtree
from tempfile import mkdtemp
from logging import getLogger
from subprocess import Popen, PIPE, STDOUT

log = getLogger(__name__)


class ArchiveController():
    def __init__(self, name, archive_config, packages):
        self.name = name
        self.archive_config = archive_config
        self.packages = packages
        self.work_dir = mkdtemp(prefix='snapshot-archive-')
        self.archive = join(self.work_dir, 'public')
        self.config = join(self.work_dir, 'config.json')

        self._install_config()
        self._create_distributions()
        self._add_packages()
        self._publish()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if isdir(self.work_dir):
            rmtree(self.work_dir)

    def _install_config(self):
        content = f"""{{
    "rootDir": "{self.work_dir}",
    "architectures": ["source", "amd64", "all"]
}}
"""

        with open(self.config, 'wb') as config:
            config.write(content.encode())

    def _create_distributions(self):
        for distrib in self.archive_config.keys():
            self._run_aptly(('repo', 'create', distrib))

    def _add_packages(self):
        for distrib, packages in self.archive_config.items():
            for package, version in packages.items():
                self._upload_package(
                    self.packages.get_changes(package, version),
                    distrib
                )

    def _upload_package(self, changes, distrib):
        self._run_aptly(('repo', 'include', '-no-remove-files',
                         '-repo', distrib, changes,))

    def _publish(self):
        for distrib in self.archive_config.keys():
            self._run_aptly(('publish', 'repo', '-distribution', distrib,
                             distrib))

        if 'unstable' in self.archive_config.keys():
            symlink('unstable',
                    join(self.work_dir, 'public', 'dists', 'sid'))

    def _run_aptly(self, args):
        proc = Popen(('aptly', '-config', self.config,) + args,
                     stdout=PIPE,
                     stderr=STDOUT)
        (output, _) = proc.communicate()

        if proc.returncode != 0:
            raise Exception('command failed:\n'
                            f'args: {args}\n'
                            f'output: {output.decode()}')

        return output.strip()
